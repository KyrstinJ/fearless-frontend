function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
        <div class="col-3">
        <div class="card h-100 row g-2 shadow p-3 mb-5">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                    <small class="text-muted">${startDate.toLocaleDateString()}-${endDate.toLocaleDateString()}</small>
            </div>
        </div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    console.log(details);
                    const title = details.conference.name;
                    const location = details.conference.location.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const endDate = new Date(details.conference.ends);
                    const html = createCard(title, description, pictureUrl, startDate, endDate, location);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }
        }
        const grid = document.querySelector('.row');
        const masonry = new Masonry(grid, {
            itemSelector: '.col-3',
            gutter: 10,
            fitWidth: false
        });

    } catch (e) {
        console.error(e);
        // Figure out what to do if an error is raised
    }
});
