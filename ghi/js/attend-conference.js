window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');
    const formTag = document.getElementById('create-attendee-form');
    const successAlert = document.getElementById('success-message');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      loadingIcon.classList.add('d-none');
    // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }
    formTag.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(object.fromEntries(formData));

      const attendeeUrl = 'http://localhost:8001/api/attendees/';
      const fetchConfig = {
        method: 'POST',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const submitResponse = await fetch(attendeeUrl, fetchConfig);
      if (submitResponse.ok) {
        formTag.requestFullscreen();
        successAlert.classList.remove('d-none');
        console.log(json);
      }
    });
  });
